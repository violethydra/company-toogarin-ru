module.exports = class AddMoveScrollUP {
	constructor(item, speed) {
		this.item = item;
		this.speed = speed;
	}

	static info() {
		console.log('MODULE:', this.name, true);
	}

	run() {
		const elem = document.querySelector(`${this.item}`);
		if (elem) {
			this.constructor.info();
			this.speed = this.speed === 'fast' ? 8 / 2 : 8;
			this.speed = this.speed === 'slow' ? 8 * 2 : 8;

			const scrollMe = () => {
				const getScroll = document.documentElement.scrollTop || document.body.scrollTop;
				if (getScroll > 0) {
					window.requestAnimationFrame(scrollMe);
					window.scrollTo(0, getScroll - getScroll / this.speed);
				}
			};
			elem.addEventListener('click', () => scrollMe());
		}
	}
};
