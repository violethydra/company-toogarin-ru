/******/ (function(modules) { // webpackBootstrap
/******/ 	// install a JSONP callback for chunk loading
/******/ 	function webpackJsonpCallback(data) {
/******/ 		var chunkIds = data[0];
/******/ 		var moreModules = data[1];
/******/ 		var executeModules = data[2];
/******/
/******/ 		// add "moreModules" to the modules object,
/******/ 		// then flag all "chunkIds" as loaded and fire callback
/******/ 		var moduleId, chunkId, i = 0, resolves = [];
/******/ 		for(;i < chunkIds.length; i++) {
/******/ 			chunkId = chunkIds[i];
/******/ 			if(installedChunks[chunkId]) {
/******/ 				resolves.push(installedChunks[chunkId][0]);
/******/ 			}
/******/ 			installedChunks[chunkId] = 0;
/******/ 		}
/******/ 		for(moduleId in moreModules) {
/******/ 			if(Object.prototype.hasOwnProperty.call(moreModules, moduleId)) {
/******/ 				modules[moduleId] = moreModules[moduleId];
/******/ 			}
/******/ 		}
/******/ 		if(parentJsonpFunction) parentJsonpFunction(data);
/******/
/******/ 		while(resolves.length) {
/******/ 			resolves.shift()();
/******/ 		}
/******/
/******/ 		// add entry modules from loaded chunk to deferred list
/******/ 		deferredModules.push.apply(deferredModules, executeModules || []);
/******/
/******/ 		// run deferred modules when all chunks ready
/******/ 		return checkDeferredModules();
/******/ 	};
/******/ 	function checkDeferredModules() {
/******/ 		var result;
/******/ 		for(var i = 0; i < deferredModules.length; i++) {
/******/ 			var deferredModule = deferredModules[i];
/******/ 			var fulfilled = true;
/******/ 			for(var j = 1; j < deferredModule.length; j++) {
/******/ 				var depId = deferredModule[j];
/******/ 				if(installedChunks[depId] !== 0) fulfilled = false;
/******/ 			}
/******/ 			if(fulfilled) {
/******/ 				deferredModules.splice(i--, 1);
/******/ 				result = __webpack_require__(__webpack_require__.s = deferredModule[0]);
/******/ 			}
/******/ 		}
/******/ 		return result;
/******/ 	}
/******/
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// object to store loaded and loading chunks
/******/ 	// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 	// Promise = chunk loading, 0 = chunk loaded
/******/ 	var installedChunks = {
/******/ 		"connect": 0
/******/ 	};
/******/
/******/ 	var deferredModules = [];
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	var jsonpArray = window["webpackJsonp"] = window["webpackJsonp"] || [];
/******/ 	var oldJsonpFunction = jsonpArray.push.bind(jsonpArray);
/******/ 	jsonpArray.push = webpackJsonpCallback;
/******/ 	jsonpArray = jsonpArray.slice();
/******/ 	for(var i = 0; i < jsonpArray.length; i++) webpackJsonpCallback(jsonpArray[i]);
/******/ 	var parentJsonpFunction = oldJsonpFunction;
/******/
/******/
/******/ 	// add entry module to deferred list
/******/ 	deferredModules.push(["./development/components/js/connect.js","plugins"]);
/******/ 	// run deferred modules when ready
/******/ 	return checkDeferredModules();
/******/ })
/************************************************************************/
/******/ ({

/***/ "./development/components/js/connect.js":
/*!**********************************************!*\
  !*** ./development/components/js/connect.js ***!
  \**********************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var owl_carousel__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! owl.carousel */ "./node_modules/owl.carousel/dist/owl.carousel.js");
/* harmony import */ var owl_carousel__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(owl_carousel__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _global__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./global */ "./development/components/js/global.js");
// ============================
//    Name: import all modules in one
// ============================



/***/ }),

/***/ "./development/components/js/global.js":
/*!*********************************************!*\
  !*** ./development/components/js/global.js ***!
  \*********************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _modules_moveScrollUP__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./modules/moveScrollUP */ "./development/components/js/modules/moveScrollUP.js");
/* harmony import */ var _modules_moveScrollUP__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_modules_moveScrollUP__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _modules_logicCarousel__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./modules/logicCarousel */ "./development/components/js/modules/logicCarousel.js");
/* harmony import */ var _modules_logicCarousel__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_modules_logicCarousel__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _modules_logicFullImage__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./modules/logicFullImage */ "./development/components/js/modules/logicFullImage.js");
/* harmony import */ var _modules_logicFullImage__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_modules_logicFullImage__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _modules_playYoutube__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./modules/playYoutube */ "./development/components/js/modules/playYoutube.js");
/* harmony import */ var _modules_playYoutube__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_modules_playYoutube__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _modules_imageZoomer__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./modules/imageZoomer */ "./development/components/js/modules/imageZoomer.js");
/* harmony import */ var _modules_imageZoomer__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_modules_imageZoomer__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _modules_tabSwitcher__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./modules/tabSwitcher */ "./development/components/js/modules/tabSwitcher.js");
/* harmony import */ var _modules_tabSwitcher__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_modules_tabSwitcher__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _modules_fetchServerData__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./modules/fetchServerData */ "./development/components/js/modules/fetchServerData.js");
/* harmony import */ var _modules_fetchServerData__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_modules_fetchServerData__WEBPACK_IMPORTED_MODULE_6__);
// ============================
//    Name: index.js
// ============================






 // import AddFetchCarouselItems from './modules/fetchCarouselItems';

var start = function start() {
  console.log('DOM:', 'DOMContentLoaded', true);
  new _modules_logicCarousel__WEBPACK_IMPORTED_MODULE_1___default.a('.js__logicCarousel', {
    sm: 1,
    md: 1,
    lg: 1,
    xl: 1,
    xxl: 1
  }, 12, false).run();
  new _modules_logicCarousel__WEBPACK_IMPORTED_MODULE_1___default.a('.js__catalogCarousel', {
    sm: 1,
    md: 1,
    lg: 2,
    xl: 5,
    xxl: 6
  }, 0, true).run();
  new _modules_logicCarousel__WEBPACK_IMPORTED_MODULE_1___default.a('.js__historyCarousel', {
    sm: 1,
    md: 1,
    lg: 2,
    xl: 5,
    xxl: 6
  }, 0, true).run();
  new _modules_logicFullImage__WEBPACK_IMPORTED_MODULE_2___default.a('.js__logicCarousel', '.js__imageTakeit').run();
  new _modules_imageZoomer__WEBPACK_IMPORTED_MODULE_4___default.a('.js__imageTakeit', '.js__imageResult').run();
  new _modules_tabSwitcher__WEBPACK_IMPORTED_MODULE_5___default.a('.js__tabsAboutIn', '.js__tabsAboutOut').run();
  new _modules_playYoutube__WEBPACK_IMPORTED_MODULE_3___default.a('.js__playYoutube').run();
  new _modules_moveScrollUP__WEBPACK_IMPORTED_MODULE_0___default.a('.js__moveScrollUP').run();
  new _modules_fetchServerData__WEBPACK_IMPORTED_MODULE_6___default.a('.js__galleryData', {
    server: 'my-server.json',
    selector: 'data-gallery-init'
  }).run(); // new AddFetchCarouselItems('.js__catalogCarousel', {
  // 	server: 'carousel-items.json',
  // 	selector: 'data-gallery-init'
  // }).run();
  // new AddtabSwitcher('.js__tabsAboutIn', '.js__tabsAboutOut').run();
};

if (typeof window !== 'undefined' && window && window.addEventListener) {
  document.addEventListener('DOMContentLoaded', start(), false);
}

/***/ }),

/***/ "./development/components/js/modules/fetchServerData.js":
/*!**************************************************************!*\
  !*** ./development/components/js/modules/fetchServerData.js ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

module.exports =
/*#__PURE__*/
function () {
  function AddFetchServerData(enter, data, exit) {
    _classCallCheck(this, AddFetchServerData);

    this.enter = enter;
    this.exit = exit;
    this.server = data.server;
    this.attr = data.selector;
  }

  _createClass(AddFetchServerData, [{
    key: "run",
    value: function run() {
      var _this = this;

      var elem = document.querySelector("".concat(this.enter)); // const output = document.querySelector(`${this.exit}`);

      if (elem) {
        this.constructor.info();
        var init = {
          method: 'GET',
          headers: {
            'Content-Type': 'application/json'
          },
          mode: 'cors',
          cache: 'default'
        };
        fetch("./".concat(this.server), init).then(function (res) {
          res.headers.get('Content-Type');
          return res.json();
        }).then(function (data) {
          var select = document.querySelectorAll("[".concat(_this.attr, "]"));

          var arr = _toConsumableArray(select).map(function (i) {
            return Object.assign(i.dataset);
          });

          var newArr = arr.map(function (x) {
            return x.galleryInit;
          });
          newArr.map(function (i) {
            return document.querySelector("[".concat(_this.attr, "=\"").concat(i, "\"]")).innerText = "".concat(data.one[i] || 'null');
          });
        }).then(function () {
          document.querySelector('.gallery__info').classList.add('no-before');
        }).catch(function (error) {
          console.log("Ouch! Fetch error: \n ".concat(error.message));
        });
      }
    }
  }], [{
    key: "info",
    value: function info() {
      console.log('MODULE:', this.name, true);
    }
  }]);

  return AddFetchServerData;
}();

/***/ }),

/***/ "./development/components/js/modules/imageZoomer.js":
/*!**********************************************************!*\
  !*** ./development/components/js/modules/imageZoomer.js ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

module.exports =
/*#__PURE__*/
function () {
  function AddImageZoomer(enter, exit) {
    _classCallCheck(this, AddImageZoomer);

    this.enter = enter;
    this.exit = exit;
  }

  _createClass(AddImageZoomer, [{
    key: "run",
    value: function run() {
      var elem = document.querySelector("".concat(this.enter));
      var result = document.querySelector("".concat(this.exit));

      if (elem) {
        this.constructor.info();

        var imageZoom = function imageZoom() {
          var lens = document.createElement('DIV');

          if (!elem.parentNode.firstElementChild.classList.contains('gallery__lenz')) {
            lens.setAttribute('class', 'gallery__lenz');
            elem.parentElement.insertBefore(lens, elem);
            result.style.visibility = 'visible';
          }

          var backgroundPos = function backgroundPos() {
            var cx = result.offsetWidth / lens.offsetWidth;
            var cy = result.offsetHeight / lens.offsetHeight;
            result.style.backgroundImage = "".concat(elem.style.backgroundImage);
            result.style.backgroundSize = "".concat(elem.offsetWidth * cx, "px ").concat(elem.offsetHeight * cy, "px");
          };

          var getCursorPos = function getCursorPos(event) {
            var a = elem.parentElement.getBoundingClientRect();
            var x = 0;
            var y = 0;
            x = event.pageX - a.left;
            y = event.pageY - a.top;
            x -= window.pageXOffset;
            y -= window.pageYOffset;
            return {
              x: x,
              y: y
            };
          };

          var moveLens = function moveLens(event) {
            event.preventDefault();
            var pos = getCursorPos(event);
            var x = pos.x - lens.offsetWidth / 2;
            var y = pos.y - lens.offsetHeight / 2;

            if (x > elem.offsetWidth - lens.offsetWidth) {
              x = elem.offsetWidth - lens.offsetWidth;
            }

            if (x < 0) {
              x = 0;
            }

            if (y > elem.offsetHeight - lens.offsetHeight) {
              y = elem.offsetHeight - lens.offsetHeight;
            }

            if (y < 0) {
              y = 0;
            }

            lens.style.left = "".concat(x, "px");
            lens.style.top = "".concat(y, "px");
            result.style.backgroundPosition = "-".concat(x * result.offsetWidth / lens.offsetWidth, "px -").concat(y * result.offsetHeight / lens.offsetHeight, "px");
          };

          backgroundPos();
          lens.addEventListener('mousemove', moveLens);
          elem.addEventListener('mousemove', moveLens);
          lens.addEventListener('touchmove', moveLens);
          elem.addEventListener('touchmove', moveLens);
        };

        var imageZoomKill = function imageZoomKill(event) {
          if (elem.parentNode.firstElementChild.classList.contains('gallery__lenz')) {
            event.target.firstElementChild.remove();
            result.style.visibility = 'hidden';
          }
        };

        elem.addEventListener('mouseenter', function (event) {
          return imageZoom(event);
        });
        elem.parentElement.addEventListener('mouseleave', function (event) {
          return imageZoomKill(event);
        });
      }
    }
  }], [{
    key: "info",
    value: function info() {
      console.log('MODULE:', this.name, true);
    }
  }]);

  return AddImageZoomer;
}();

/***/ }),

/***/ "./development/components/js/modules/logicCarousel.js":
/*!************************************************************!*\
  !*** ./development/components/js/modules/logicCarousel.js ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function($) {function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

module.exports =
/*#__PURE__*/
function () {
  function AddLogicCarousel(enter, init, gap, drag) {
    _classCallCheck(this, AddLogicCarousel);

    this.enter = enter;
    this.drag = drag;
    this.gap = gap;
    this.init = {
      0: {
        items: init.sm
      },
      768: {
        items: init.md
      },
      992: {
        items: init.lg
      },
      1200: {
        items: init.xl
      },
      1600: {
        items: init.xxl
      }
    };
  }

  _createClass(AddLogicCarousel, [{
    key: "run",
    value: function run() {
      var _this = this;

      var elem = document.querySelector("".concat(this.enter)); // const output = document.querySelector(`${this.exit}`);

      if (elem) {
        this.constructor.info();
        var init = {
          loop: true,
          margin: this.gap,
          nav: false,
          dots: false,
          rewind: false,
          touchDrag: this.drag,
          mouseDrag: this.drag,
          merge: false,
          responsive: this.init
        };
        $(this.enter).owlCarousel(init);
        elem.style.visibility = 'visible';

        var control = function control(event, name) {
          event.preventDefault();
          $(_this.enter).trigger("".concat(name, ".owl.carousel"));
        };

        $("".concat(this.enter, "Navigation")).eq(1).on('click', function (event) {
          return control(event, 'next');
        });
        $("".concat(this.enter, "Navigation")).eq(0).on('click', function (event) {
          return control(event, 'prev');
        });
      }
    }
  }], [{
    key: "info",
    value: function info() {
      console.log('MODULE:', this.name, true);
    }
  }]);

  return AddLogicCarousel;
}();
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./development/components/js/modules/logicFullImage.js":
/*!*************************************************************!*\
  !*** ./development/components/js/modules/logicFullImage.js ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function($) {function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

module.exports =
/*#__PURE__*/
function () {
  function AddLogicFullImage(enter, exit) {
    _classCallCheck(this, AddLogicFullImage);

    this.enter = enter;
    this.exit = exit;
  }

  _createClass(AddLogicFullImage, [{
    key: "run",
    value: function run() {
      var elem = document.querySelector("".concat(this.enter));
      var output = document.querySelector("".concat(this.exit));

      if (elem) {
        this.constructor.info();

        var clickedFunc = function clickedFunc(event) {
          event.preventDefault();
          var curElem = event.target;
          var getData = curElem.src || curElem.firstElementChild.src;
          output.style.backgroundImage = "url(".concat(getData, ")");
        };

        var mainUrl = _toConsumableArray(elem.querySelector('.owl-item').parentElement.children)[4];

        var url = mainUrl.querySelector('img').src;
        output.style.backgroundImage = "url(".concat(url, ")");
        output.style.width = "".concat(output.parentElement.offsetWidth, "px");
        $("".concat(this.enter, " .gallery__item")).on('click', function (event) {
          return clickedFunc(event);
        });
      }
    }
  }], [{
    key: "info",
    value: function info() {
      console.log('MODULE:', this.name, true);
    }
  }]);

  return AddLogicFullImage;
}();
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./development/components/js/modules/moveScrollUP.js":
/*!***********************************************************!*\
  !*** ./development/components/js/modules/moveScrollUP.js ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

module.exports =
/*#__PURE__*/
function () {
  function AddMoveScrollUP(item, speed) {
    _classCallCheck(this, AddMoveScrollUP);

    this.item = item;
    this.speed = speed;
  }

  _createClass(AddMoveScrollUP, [{
    key: "run",
    value: function run() {
      var _this = this;

      var elem = document.querySelector("".concat(this.item));

      if (elem) {
        this.constructor.info();
        this.speed = this.speed === 'fast' ? 8 / 2 : 8;
        this.speed = this.speed === 'slow' ? 8 * 2 : 8;

        var scrollMe = function scrollMe() {
          var getScroll = document.documentElement.scrollTop || document.body.scrollTop;

          if (getScroll > 0) {
            window.requestAnimationFrame(scrollMe);
            window.scrollTo(0, getScroll - getScroll / _this.speed);
          }
        };

        elem.addEventListener('click', function () {
          return scrollMe();
        });
      }
    }
  }], [{
    key: "info",
    value: function info() {
      console.log('MODULE:', this.name, true);
    }
  }]);

  return AddMoveScrollUP;
}();

/***/ }),

/***/ "./development/components/js/modules/playYoutube.js":
/*!**********************************************************!*\
  !*** ./development/components/js/modules/playYoutube.js ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function($) {function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

module.exports =
/*#__PURE__*/
function () {
  function AddPlayYoutube(item, number, drag) {
    _classCallCheck(this, AddPlayYoutube);

    this.item = item;
    this.number = number;
    this.drag = drag;
  }

  _createClass(AddPlayYoutube, [{
    key: "run",
    value: function run() {
      var _this = this;

      var elem = document.querySelector("".concat(this.item));

      if (elem) {
        this.constructor.info();

        var doit = function doit(event) {
          var qwe = $('#youtube-start-url').attr('data-lazy-src');
          $('#youtube-start-url').attr('src', qwe);
          $("".concat(_this.item)).fadeOut();
        };

        elem.addEventListener('click', function (event) {
          return doit(event);
        });
      }
    }
  }], [{
    key: "info",
    value: function info() {
      console.log('MODULE:', this.name, true);
    }
  }]);

  return AddPlayYoutube;
}();
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./development/components/js/modules/tabSwitcher.js":
/*!**********************************************************!*\
  !*** ./development/components/js/modules/tabSwitcher.js ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

module.exports =
/*#__PURE__*/
function () {
  function AddtabSwitcher(enter, exit) {
    _classCallCheck(this, AddtabSwitcher);

    this.enter = enter;
    this.exit = exit;
  }

  _createClass(AddtabSwitcher, [{
    key: "run",
    value: function run() {
      var elem = document.querySelector("".concat(this.enter));
      var output = document.querySelector("".concat(this.exit));

      if (elem) {
        this.constructor.info();

        var logic = function logic(event) {
          event.preventDefault();
          var curElem = event.target;

          var index = _toConsumableArray(curElem.parentElement.children).indexOf(curElem);

          if (output.querySelector('.active')) {
            output.querySelector('.active').classList.remove('active');
          }

          output.children[index].classList.add('active');
        };

        output.firstElementChild.classList.add('active');
        elem.addEventListener('click', function (event) {
          return logic(event);
        });
      }
    }
  }], [{
    key: "info",
    value: function info() {
      console.log('MODULE:', this.name, true);
    }
  }]);

  return AddtabSwitcher;
}();

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29ubmVjdC5qcyIsInNvdXJjZXMiOlsid2VicGFjazovLy93ZWJwYWNrL2Jvb3RzdHJhcCIsIndlYnBhY2s6Ly8vLi9kZXZlbG9wbWVudC9jb21wb25lbnRzL2pzL2Nvbm5lY3QuanMiLCJ3ZWJwYWNrOi8vLy4vZGV2ZWxvcG1lbnQvY29tcG9uZW50cy9qcy9nbG9iYWwuanMiLCJ3ZWJwYWNrOi8vLy4vZGV2ZWxvcG1lbnQvY29tcG9uZW50cy9qcy9tb2R1bGVzL2ZldGNoU2VydmVyRGF0YS5qcyIsIndlYnBhY2s6Ly8vLi9kZXZlbG9wbWVudC9jb21wb25lbnRzL2pzL21vZHVsZXMvaW1hZ2Vab29tZXIuanMiLCJ3ZWJwYWNrOi8vLy4vZGV2ZWxvcG1lbnQvY29tcG9uZW50cy9qcy9tb2R1bGVzL2xvZ2ljQ2Fyb3VzZWwuanMiLCJ3ZWJwYWNrOi8vLy4vZGV2ZWxvcG1lbnQvY29tcG9uZW50cy9qcy9tb2R1bGVzL2xvZ2ljRnVsbEltYWdlLmpzIiwid2VicGFjazovLy8uL2RldmVsb3BtZW50L2NvbXBvbmVudHMvanMvbW9kdWxlcy9tb3ZlU2Nyb2xsVVAuanMiLCJ3ZWJwYWNrOi8vLy4vZGV2ZWxvcG1lbnQvY29tcG9uZW50cy9qcy9tb2R1bGVzL3BsYXlZb3V0dWJlLmpzIiwid2VicGFjazovLy8uL2RldmVsb3BtZW50L2NvbXBvbmVudHMvanMvbW9kdWxlcy90YWJTd2l0Y2hlci5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBpbnN0YWxsIGEgSlNPTlAgY2FsbGJhY2sgZm9yIGNodW5rIGxvYWRpbmdcbiBcdGZ1bmN0aW9uIHdlYnBhY2tKc29ucENhbGxiYWNrKGRhdGEpIHtcbiBcdFx0dmFyIGNodW5rSWRzID0gZGF0YVswXTtcbiBcdFx0dmFyIG1vcmVNb2R1bGVzID0gZGF0YVsxXTtcbiBcdFx0dmFyIGV4ZWN1dGVNb2R1bGVzID0gZGF0YVsyXTtcblxuIFx0XHQvLyBhZGQgXCJtb3JlTW9kdWxlc1wiIHRvIHRoZSBtb2R1bGVzIG9iamVjdCxcbiBcdFx0Ly8gdGhlbiBmbGFnIGFsbCBcImNodW5rSWRzXCIgYXMgbG9hZGVkIGFuZCBmaXJlIGNhbGxiYWNrXG4gXHRcdHZhciBtb2R1bGVJZCwgY2h1bmtJZCwgaSA9IDAsIHJlc29sdmVzID0gW107XG4gXHRcdGZvcig7aSA8IGNodW5rSWRzLmxlbmd0aDsgaSsrKSB7XG4gXHRcdFx0Y2h1bmtJZCA9IGNodW5rSWRzW2ldO1xuIFx0XHRcdGlmKGluc3RhbGxlZENodW5rc1tjaHVua0lkXSkge1xuIFx0XHRcdFx0cmVzb2x2ZXMucHVzaChpbnN0YWxsZWRDaHVua3NbY2h1bmtJZF1bMF0pO1xuIFx0XHRcdH1cbiBcdFx0XHRpbnN0YWxsZWRDaHVua3NbY2h1bmtJZF0gPSAwO1xuIFx0XHR9XG4gXHRcdGZvcihtb2R1bGVJZCBpbiBtb3JlTW9kdWxlcykge1xuIFx0XHRcdGlmKE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChtb3JlTW9kdWxlcywgbW9kdWxlSWQpKSB7XG4gXHRcdFx0XHRtb2R1bGVzW21vZHVsZUlkXSA9IG1vcmVNb2R1bGVzW21vZHVsZUlkXTtcbiBcdFx0XHR9XG4gXHRcdH1cbiBcdFx0aWYocGFyZW50SnNvbnBGdW5jdGlvbikgcGFyZW50SnNvbnBGdW5jdGlvbihkYXRhKTtcblxuIFx0XHR3aGlsZShyZXNvbHZlcy5sZW5ndGgpIHtcbiBcdFx0XHRyZXNvbHZlcy5zaGlmdCgpKCk7XG4gXHRcdH1cblxuIFx0XHQvLyBhZGQgZW50cnkgbW9kdWxlcyBmcm9tIGxvYWRlZCBjaHVuayB0byBkZWZlcnJlZCBsaXN0XG4gXHRcdGRlZmVycmVkTW9kdWxlcy5wdXNoLmFwcGx5KGRlZmVycmVkTW9kdWxlcywgZXhlY3V0ZU1vZHVsZXMgfHwgW10pO1xuXG4gXHRcdC8vIHJ1biBkZWZlcnJlZCBtb2R1bGVzIHdoZW4gYWxsIGNodW5rcyByZWFkeVxuIFx0XHRyZXR1cm4gY2hlY2tEZWZlcnJlZE1vZHVsZXMoKTtcbiBcdH07XG4gXHRmdW5jdGlvbiBjaGVja0RlZmVycmVkTW9kdWxlcygpIHtcbiBcdFx0dmFyIHJlc3VsdDtcbiBcdFx0Zm9yKHZhciBpID0gMDsgaSA8IGRlZmVycmVkTW9kdWxlcy5sZW5ndGg7IGkrKykge1xuIFx0XHRcdHZhciBkZWZlcnJlZE1vZHVsZSA9IGRlZmVycmVkTW9kdWxlc1tpXTtcbiBcdFx0XHR2YXIgZnVsZmlsbGVkID0gdHJ1ZTtcbiBcdFx0XHRmb3IodmFyIGogPSAxOyBqIDwgZGVmZXJyZWRNb2R1bGUubGVuZ3RoOyBqKyspIHtcbiBcdFx0XHRcdHZhciBkZXBJZCA9IGRlZmVycmVkTW9kdWxlW2pdO1xuIFx0XHRcdFx0aWYoaW5zdGFsbGVkQ2h1bmtzW2RlcElkXSAhPT0gMCkgZnVsZmlsbGVkID0gZmFsc2U7XG4gXHRcdFx0fVxuIFx0XHRcdGlmKGZ1bGZpbGxlZCkge1xuIFx0XHRcdFx0ZGVmZXJyZWRNb2R1bGVzLnNwbGljZShpLS0sIDEpO1xuIFx0XHRcdFx0cmVzdWx0ID0gX193ZWJwYWNrX3JlcXVpcmVfXyhfX3dlYnBhY2tfcmVxdWlyZV9fLnMgPSBkZWZlcnJlZE1vZHVsZVswXSk7XG4gXHRcdFx0fVxuIFx0XHR9XG4gXHRcdHJldHVybiByZXN1bHQ7XG4gXHR9XG5cbiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIG9iamVjdCB0byBzdG9yZSBsb2FkZWQgYW5kIGxvYWRpbmcgY2h1bmtzXG4gXHQvLyB1bmRlZmluZWQgPSBjaHVuayBub3QgbG9hZGVkLCBudWxsID0gY2h1bmsgcHJlbG9hZGVkL3ByZWZldGNoZWRcbiBcdC8vIFByb21pc2UgPSBjaHVuayBsb2FkaW5nLCAwID0gY2h1bmsgbG9hZGVkXG4gXHR2YXIgaW5zdGFsbGVkQ2h1bmtzID0ge1xuIFx0XHRcImNvbm5lY3RcIjogMFxuIFx0fTtcblxuIFx0dmFyIGRlZmVycmVkTW9kdWxlcyA9IFtdO1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9uIGZvciBoYXJtb255IGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuIFx0XHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIG5hbWUpKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIG5hbWUsIHsgZW51bWVyYWJsZTogdHJ1ZSwgZ2V0OiBnZXR0ZXIgfSk7XG4gXHRcdH1cbiBcdH07XG5cbiBcdC8vIGRlZmluZSBfX2VzTW9kdWxlIG9uIGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uciA9IGZ1bmN0aW9uKGV4cG9ydHMpIHtcbiBcdFx0aWYodHlwZW9mIFN5bWJvbCAhPT0gJ3VuZGVmaW5lZCcgJiYgU3ltYm9sLnRvU3RyaW5nVGFnKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFN5bWJvbC50b1N0cmluZ1RhZywgeyB2YWx1ZTogJ01vZHVsZScgfSk7XG4gXHRcdH1cbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdfX2VzTW9kdWxlJywgeyB2YWx1ZTogdHJ1ZSB9KTtcbiBcdH07XG5cbiBcdC8vIGNyZWF0ZSBhIGZha2UgbmFtZXNwYWNlIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDE6IHZhbHVlIGlzIGEgbW9kdWxlIGlkLCByZXF1aXJlIGl0XG4gXHQvLyBtb2RlICYgMjogbWVyZ2UgYWxsIHByb3BlcnRpZXMgb2YgdmFsdWUgaW50byB0aGUgbnNcbiBcdC8vIG1vZGUgJiA0OiByZXR1cm4gdmFsdWUgd2hlbiBhbHJlYWR5IG5zIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDh8MTogYmVoYXZlIGxpa2UgcmVxdWlyZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy50ID0gZnVuY3Rpb24odmFsdWUsIG1vZGUpIHtcbiBcdFx0aWYobW9kZSAmIDEpIHZhbHVlID0gX193ZWJwYWNrX3JlcXVpcmVfXyh2YWx1ZSk7XG4gXHRcdGlmKG1vZGUgJiA4KSByZXR1cm4gdmFsdWU7XG4gXHRcdGlmKChtb2RlICYgNCkgJiYgdHlwZW9mIHZhbHVlID09PSAnb2JqZWN0JyAmJiB2YWx1ZSAmJiB2YWx1ZS5fX2VzTW9kdWxlKSByZXR1cm4gdmFsdWU7XG4gXHRcdHZhciBucyA9IE9iamVjdC5jcmVhdGUobnVsbCk7XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18ucihucyk7XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShucywgJ2RlZmF1bHQnLCB7IGVudW1lcmFibGU6IHRydWUsIHZhbHVlOiB2YWx1ZSB9KTtcbiBcdFx0aWYobW9kZSAmIDIgJiYgdHlwZW9mIHZhbHVlICE9ICdzdHJpbmcnKSBmb3IodmFyIGtleSBpbiB2YWx1ZSkgX193ZWJwYWNrX3JlcXVpcmVfXy5kKG5zLCBrZXksIGZ1bmN0aW9uKGtleSkgeyByZXR1cm4gdmFsdWVba2V5XTsgfS5iaW5kKG51bGwsIGtleSkpO1xuIFx0XHRyZXR1cm4gbnM7XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG4gXHR2YXIganNvbnBBcnJheSA9IHdpbmRvd1tcIndlYnBhY2tKc29ucFwiXSA9IHdpbmRvd1tcIndlYnBhY2tKc29ucFwiXSB8fCBbXTtcbiBcdHZhciBvbGRKc29ucEZ1bmN0aW9uID0ganNvbnBBcnJheS5wdXNoLmJpbmQoanNvbnBBcnJheSk7XG4gXHRqc29ucEFycmF5LnB1c2ggPSB3ZWJwYWNrSnNvbnBDYWxsYmFjaztcbiBcdGpzb25wQXJyYXkgPSBqc29ucEFycmF5LnNsaWNlKCk7XG4gXHRmb3IodmFyIGkgPSAwOyBpIDwganNvbnBBcnJheS5sZW5ndGg7IGkrKykgd2VicGFja0pzb25wQ2FsbGJhY2soanNvbnBBcnJheVtpXSk7XG4gXHR2YXIgcGFyZW50SnNvbnBGdW5jdGlvbiA9IG9sZEpzb25wRnVuY3Rpb247XG5cblxuIFx0Ly8gYWRkIGVudHJ5IG1vZHVsZSB0byBkZWZlcnJlZCBsaXN0XG4gXHRkZWZlcnJlZE1vZHVsZXMucHVzaChbXCIuL2RldmVsb3BtZW50L2NvbXBvbmVudHMvanMvY29ubmVjdC5qc1wiLFwicGx1Z2luc1wiXSk7XG4gXHQvLyBydW4gZGVmZXJyZWQgbW9kdWxlcyB3aGVuIHJlYWR5XG4gXHRyZXR1cm4gY2hlY2tEZWZlcnJlZE1vZHVsZXMoKTtcbiIsIi8vID09PT09PT09PT09PT09PT09PT09PT09PT09PT1cclxuLy8gICAgTmFtZTogaW1wb3J0IGFsbCBtb2R1bGVzIGluIG9uZVxyXG4vLyA9PT09PT09PT09PT09PT09PT09PT09PT09PT09XHJcblxyXG5pbXBvcnQgJ293bC5jYXJvdXNlbCc7XHJcbmltcG9ydCAnLi9nbG9iYWwnO1xyXG4iLCIvLyA9PT09PT09PT09PT09PT09PT09PT09PT09PT09XHJcbi8vICAgIE5hbWU6IGluZGV4LmpzXHJcbi8vID09PT09PT09PT09PT09PT09PT09PT09PT09PT1cclxuXHJcbmltcG9ydCBBZGRNb3ZlU2Nyb2xsVVAgZnJvbSAnLi9tb2R1bGVzL21vdmVTY3JvbGxVUCc7XHJcbmltcG9ydCBBZGRMb2dpY0Nhcm91c2VsIGZyb20gJy4vbW9kdWxlcy9sb2dpY0Nhcm91c2VsJztcclxuaW1wb3J0IEFkZExvZ2ljRnVsbEltYWdlIGZyb20gJy4vbW9kdWxlcy9sb2dpY0Z1bGxJbWFnZSc7XHJcbmltcG9ydCBBZGRQbGF5WW91dHViZSBmcm9tICcuL21vZHVsZXMvcGxheVlvdXR1YmUnO1xyXG5pbXBvcnQgQWRkSW1hZ2Vab29tZXIgZnJvbSAnLi9tb2R1bGVzL2ltYWdlWm9vbWVyJztcclxuaW1wb3J0IEFkZFRhYlN3aXRjaGVyIGZyb20gJy4vbW9kdWxlcy90YWJTd2l0Y2hlcic7XHJcbmltcG9ydCBBZGRGZXRjaFNlcnZlckRhdGEgZnJvbSAnLi9tb2R1bGVzL2ZldGNoU2VydmVyRGF0YSc7XHJcblxyXG4vLyBpbXBvcnQgQWRkRmV0Y2hDYXJvdXNlbEl0ZW1zIGZyb20gJy4vbW9kdWxlcy9mZXRjaENhcm91c2VsSXRlbXMnO1xyXG5cclxuY29uc3Qgc3RhcnQgPSAoKSA9PiB7XHJcblx0Y29uc29sZS5sb2coJ0RPTTonLCAnRE9NQ29udGVudExvYWRlZCcsIHRydWUpO1xyXG5cclxuXHRuZXcgQWRkTG9naWNDYXJvdXNlbCgnLmpzX19sb2dpY0Nhcm91c2VsJywgeyBzbTogMSwgbWQ6IDEsIGxnOiAxLCB4bDogMSwgeHhsOiAxIH0sIDEyLCBmYWxzZSkucnVuKCk7XHJcblx0bmV3IEFkZExvZ2ljQ2Fyb3VzZWwoJy5qc19fY2F0YWxvZ0Nhcm91c2VsJywgeyBzbTogMSwgbWQ6IDEsIGxnOiAyLCB4bDogNSwgeHhsOiA2IH0sIDAsIHRydWUpLnJ1bigpO1xyXG5cdG5ldyBBZGRMb2dpY0Nhcm91c2VsKCcuanNfX2hpc3RvcnlDYXJvdXNlbCcsIHsgc206IDEsIG1kOiAxLCBsZzogMiwgeGw6IDUsIHh4bDogNiB9LCAwLCB0cnVlKS5ydW4oKTtcclxuXHJcblx0bmV3IEFkZExvZ2ljRnVsbEltYWdlKCcuanNfX2xvZ2ljQ2Fyb3VzZWwnLCAnLmpzX19pbWFnZVRha2VpdCcpLnJ1bigpO1xyXG5cdG5ldyBBZGRJbWFnZVpvb21lcignLmpzX19pbWFnZVRha2VpdCcsICcuanNfX2ltYWdlUmVzdWx0JykucnVuKCk7XHJcblx0bmV3IEFkZFRhYlN3aXRjaGVyKCcuanNfX3RhYnNBYm91dEluJywgJy5qc19fdGFic0Fib3V0T3V0JykucnVuKCk7XHJcblx0bmV3IEFkZFBsYXlZb3V0dWJlKCcuanNfX3BsYXlZb3V0dWJlJykucnVuKCk7XHJcblx0bmV3IEFkZE1vdmVTY3JvbGxVUCgnLmpzX19tb3ZlU2Nyb2xsVVAnKS5ydW4oKTtcclxuXHJcblx0bmV3IEFkZEZldGNoU2VydmVyRGF0YSgnLmpzX19nYWxsZXJ5RGF0YScsIHtcclxuXHRcdHNlcnZlcjogJ215LXNlcnZlci5qc29uJyxcclxuXHRcdHNlbGVjdG9yOiAnZGF0YS1nYWxsZXJ5LWluaXQnXHJcblx0fSkucnVuKCk7XHJcblxyXG5cdC8vIG5ldyBBZGRGZXRjaENhcm91c2VsSXRlbXMoJy5qc19fY2F0YWxvZ0Nhcm91c2VsJywge1xyXG5cdC8vIFx0c2VydmVyOiAnY2Fyb3VzZWwtaXRlbXMuanNvbicsXHJcblx0Ly8gXHRzZWxlY3RvcjogJ2RhdGEtZ2FsbGVyeS1pbml0J1xyXG5cdC8vIH0pLnJ1bigpO1xyXG5cclxuXHQvLyBuZXcgQWRkdGFiU3dpdGNoZXIoJy5qc19fdGFic0Fib3V0SW4nLCAnLmpzX190YWJzQWJvdXRPdXQnKS5ydW4oKTtcclxufTtcclxuaWYgKHR5cGVvZiB3aW5kb3cgIT09ICd1bmRlZmluZWQnICYmIHdpbmRvdyAmJiB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcikge1xyXG5cdGRvY3VtZW50LmFkZEV2ZW50TGlzdGVuZXIoJ0RPTUNvbnRlbnRMb2FkZWQnLCBzdGFydCgpLCBmYWxzZSk7XHJcbn1cclxuIiwibW9kdWxlLmV4cG9ydHMgPSBjbGFzcyBBZGRGZXRjaFNlcnZlckRhdGEge1xyXG5cdGNvbnN0cnVjdG9yKGVudGVyLCBkYXRhLCBleGl0KSB7XHJcblx0XHR0aGlzLmVudGVyID0gZW50ZXI7XHJcblx0XHR0aGlzLmV4aXQgPSBleGl0O1xyXG5cdFx0dGhpcy5zZXJ2ZXIgPSBkYXRhLnNlcnZlcjtcclxuXHRcdHRoaXMuYXR0ciA9IGRhdGEuc2VsZWN0b3I7XHJcblx0fVxyXG5cclxuXHRzdGF0aWMgaW5mbygpIHtcclxuXHRcdGNvbnNvbGUubG9nKCdNT0RVTEU6JywgdGhpcy5uYW1lLCB0cnVlKTtcclxuXHR9XHJcblxyXG5cdHJ1bigpIHtcclxuXHRcdGNvbnN0IGVsZW0gPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKGAke3RoaXMuZW50ZXJ9YCk7XHJcblx0XHQvLyBjb25zdCBvdXRwdXQgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKGAke3RoaXMuZXhpdH1gKTtcclxuXHRcdGlmIChlbGVtKSB7XHJcblx0XHRcdHRoaXMuY29uc3RydWN0b3IuaW5mbygpO1xyXG5cclxuXHRcdFx0Y29uc3QgaW5pdCA9IHtcclxuXHRcdFx0XHRtZXRob2Q6ICdHRVQnLFxyXG5cdFx0XHRcdGhlYWRlcnM6IHsgJ0NvbnRlbnQtVHlwZSc6ICdhcHBsaWNhdGlvbi9qc29uJyB9LFxyXG5cdFx0XHRcdG1vZGU6ICdjb3JzJyxcclxuXHRcdFx0XHRjYWNoZTogJ2RlZmF1bHQnXHJcblx0XHRcdH07XHJcblxyXG5cdFx0XHRmZXRjaChgLi8ke3RoaXMuc2VydmVyfWAsIGluaXQpXHJcblx0XHRcdFx0LnRoZW4oKHJlcykgPT4ge1xyXG5cdFx0XHRcdFx0cmVzLmhlYWRlcnMuZ2V0KCdDb250ZW50LVR5cGUnKTtcclxuXHRcdFx0XHRcdHJldHVybiByZXMuanNvbigpO1xyXG5cdFx0XHRcdH0pXHJcblx0XHRcdFx0LnRoZW4oKGRhdGEpID0+IHtcclxuXHRcdFx0XHRcdGNvbnN0IHNlbGVjdCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoYFske3RoaXMuYXR0cn1dYCk7XHJcblx0XHRcdFx0XHRjb25zdCBhcnIgPSBbLi4uc2VsZWN0XS5tYXAoaSA9PiAoT2JqZWN0LmFzc2lnbihpLmRhdGFzZXQpKSk7XHJcblx0XHRcdFx0XHRjb25zdCBuZXdBcnIgPSBhcnIubWFwKHggPT4geC5nYWxsZXJ5SW5pdCk7XHJcblx0XHRcdFx0XHRuZXdBcnIubWFwKGkgPT4gKFxyXG5cdFx0XHRcdFx0XHRkb2N1bWVudC5xdWVyeVNlbGVjdG9yKGBbJHt0aGlzLmF0dHJ9PVwiJHtpfVwiXWApLmlubmVyVGV4dCA9IGAke2RhdGEub25lW2ldIHx8ICdudWxsJ31gKSk7XHJcblx0XHRcdFx0fSlcclxuXHRcdFx0XHQudGhlbigoKSA9PiB7XHJcblx0XHRcdFx0XHRkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcuZ2FsbGVyeV9faW5mbycpLmNsYXNzTGlzdC5hZGQoJ25vLWJlZm9yZScpO1xyXG5cdFx0XHRcdH0pXHJcblx0XHRcdFx0LmNhdGNoKChlcnJvcikgPT4ge1xyXG5cdFx0XHRcdFx0Y29uc29sZS5sb2coYE91Y2ghIEZldGNoIGVycm9yOiBcXG4gJHtlcnJvci5tZXNzYWdlfWApO1xyXG5cdFx0XHRcdH0pO1xyXG5cdFx0fVxyXG5cdH1cclxufTtcclxuIiwibW9kdWxlLmV4cG9ydHMgPSBjbGFzcyBBZGRJbWFnZVpvb21lciB7XHJcblx0Y29uc3RydWN0b3IoZW50ZXIsIGV4aXQpIHtcclxuXHRcdHRoaXMuZW50ZXIgPSBlbnRlcjtcclxuXHRcdHRoaXMuZXhpdCA9IGV4aXQ7XHJcblx0fVxyXG5cclxuXHRzdGF0aWMgaW5mbygpIHtcclxuXHRcdGNvbnNvbGUubG9nKCdNT0RVTEU6JywgdGhpcy5uYW1lLCB0cnVlKTtcclxuXHR9XHJcblxyXG5cdHJ1bigpIHtcclxuXHRcdGNvbnN0IGVsZW0gPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKGAke3RoaXMuZW50ZXJ9YCk7XHJcblx0XHRjb25zdCByZXN1bHQgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKGAke3RoaXMuZXhpdH1gKTtcclxuXHRcdGlmIChlbGVtKSB7XHJcblx0XHRcdHRoaXMuY29uc3RydWN0b3IuaW5mbygpO1xyXG5cclxuXHRcdFx0Y29uc3QgaW1hZ2Vab29tID0gKCkgPT4ge1xyXG5cdFx0XHRcdGNvbnN0IGxlbnMgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdESVYnKTtcclxuXHJcblx0XHRcdFx0aWYgKCFlbGVtLnBhcmVudE5vZGUuZmlyc3RFbGVtZW50Q2hpbGQuY2xhc3NMaXN0LmNvbnRhaW5zKCdnYWxsZXJ5X19sZW56JykpIHtcclxuXHRcdFx0XHRcdGxlbnMuc2V0QXR0cmlidXRlKCdjbGFzcycsICdnYWxsZXJ5X19sZW56Jyk7XHJcblx0XHRcdFx0XHRlbGVtLnBhcmVudEVsZW1lbnQuaW5zZXJ0QmVmb3JlKGxlbnMsIGVsZW0pO1xyXG5cdFx0XHRcdFx0cmVzdWx0LnN0eWxlLnZpc2liaWxpdHkgPSAndmlzaWJsZSc7XHJcblx0XHRcdFx0fVxyXG5cclxuXHRcdFx0XHRjb25zdCBiYWNrZ3JvdW5kUG9zID0gKCkgPT4ge1xyXG5cdFx0XHRcdFx0Y29uc3QgY3ggPSByZXN1bHQub2Zmc2V0V2lkdGggLyBsZW5zLm9mZnNldFdpZHRoO1xyXG5cdFx0XHRcdFx0Y29uc3QgY3kgPSByZXN1bHQub2Zmc2V0SGVpZ2h0IC8gbGVucy5vZmZzZXRIZWlnaHQ7XHJcblx0XHRcdFx0XHRyZXN1bHQuc3R5bGUuYmFja2dyb3VuZEltYWdlID0gYCR7ZWxlbS5zdHlsZS5iYWNrZ3JvdW5kSW1hZ2V9YDtcclxuXHRcdFx0XHRcdHJlc3VsdC5zdHlsZS5iYWNrZ3JvdW5kU2l6ZSA9IGAkeyhlbGVtLm9mZnNldFdpZHRoICogY3gpfXB4ICR7KGVsZW0ub2Zmc2V0SGVpZ2h0ICogY3kpfXB4YDtcclxuXHRcdFx0XHR9O1xyXG5cclxuXHRcdFx0XHRjb25zdCBnZXRDdXJzb3JQb3MgPSAoZXZlbnQpID0+IHtcclxuXHRcdFx0XHRcdGNvbnN0IGEgPSBlbGVtLnBhcmVudEVsZW1lbnQuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCk7XHJcblx0XHRcdFx0XHRsZXQgeCA9IDA7XHJcblx0XHRcdFx0XHRsZXQgeSA9IDA7XHJcblxyXG5cdFx0XHRcdFx0eCA9IGV2ZW50LnBhZ2VYIC0gYS5sZWZ0O1xyXG5cdFx0XHRcdFx0eSA9IGV2ZW50LnBhZ2VZIC0gYS50b3A7XHJcblxyXG5cdFx0XHRcdFx0eCAtPSB3aW5kb3cucGFnZVhPZmZzZXQ7XHJcblx0XHRcdFx0XHR5IC09IHdpbmRvdy5wYWdlWU9mZnNldDtcclxuXHJcblx0XHRcdFx0XHRyZXR1cm4geyB4LCB5IH07XHJcblx0XHRcdFx0fTtcclxuXHJcblx0XHRcdFx0Y29uc3QgbW92ZUxlbnMgPSAoZXZlbnQpID0+IHtcclxuXHRcdFx0XHRcdGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XHJcblxyXG5cdFx0XHRcdFx0Y29uc3QgcG9zID0gZ2V0Q3Vyc29yUG9zKGV2ZW50KTtcclxuXHRcdFx0XHRcdGxldCB4ID0gcG9zLnggLSAobGVucy5vZmZzZXRXaWR0aCAvIDIpO1xyXG5cdFx0XHRcdFx0bGV0IHkgPSBwb3MueSAtIChsZW5zLm9mZnNldEhlaWdodCAvIDIpO1xyXG5cclxuXHRcdFx0XHRcdGlmICh4ID4gZWxlbS5vZmZzZXRXaWR0aCAtIGxlbnMub2Zmc2V0V2lkdGgpIHsgeCA9IGVsZW0ub2Zmc2V0V2lkdGggLSBsZW5zLm9mZnNldFdpZHRoOyB9XHJcblx0XHRcdFx0XHRpZiAoeCA8IDApIHsgeCA9IDA7IH1cclxuXHRcdFx0XHRcdGlmICh5ID4gZWxlbS5vZmZzZXRIZWlnaHQgLSBsZW5zLm9mZnNldEhlaWdodCkgeyB5ID0gZWxlbS5vZmZzZXRIZWlnaHQgLSBsZW5zLm9mZnNldEhlaWdodDsgfVxyXG5cdFx0XHRcdFx0aWYgKHkgPCAwKSB7IHkgPSAwOyB9XHJcblxyXG5cdFx0XHRcdFx0bGVucy5zdHlsZS5sZWZ0ID0gYCR7eH1weGA7XHJcblx0XHRcdFx0XHRsZW5zLnN0eWxlLnRvcCA9IGAke3l9cHhgO1xyXG5cclxuXHRcdFx0XHRcdHJlc3VsdC5zdHlsZS5iYWNrZ3JvdW5kUG9zaXRpb24gPSBgLSR7KHggKiByZXN1bHQub2Zmc2V0V2lkdGggLyBsZW5zLm9mZnNldFdpZHRoKX1weCAtJHsoeSAqIHJlc3VsdC5vZmZzZXRIZWlnaHQgLyBsZW5zLm9mZnNldEhlaWdodCl9cHhgO1xyXG5cdFx0XHRcdH07XHJcblxyXG5cdFx0XHRcdGJhY2tncm91bmRQb3MoKTtcclxuXHRcdFx0XHRsZW5zLmFkZEV2ZW50TGlzdGVuZXIoJ21vdXNlbW92ZScsIG1vdmVMZW5zKTtcclxuXHRcdFx0XHRlbGVtLmFkZEV2ZW50TGlzdGVuZXIoJ21vdXNlbW92ZScsIG1vdmVMZW5zKTtcclxuXHRcdFx0XHRsZW5zLmFkZEV2ZW50TGlzdGVuZXIoJ3RvdWNobW92ZScsIG1vdmVMZW5zKTtcclxuXHRcdFx0XHRlbGVtLmFkZEV2ZW50TGlzdGVuZXIoJ3RvdWNobW92ZScsIG1vdmVMZW5zKTtcclxuXHRcdFx0fTtcclxuXHJcblx0XHRcdGNvbnN0IGltYWdlWm9vbUtpbGwgPSAoZXZlbnQpID0+IHtcclxuXHRcdFx0XHRpZiAoZWxlbS5wYXJlbnROb2RlLmZpcnN0RWxlbWVudENoaWxkLmNsYXNzTGlzdC5jb250YWlucygnZ2FsbGVyeV9fbGVueicpKSB7XHJcblx0XHRcdFx0XHRldmVudC50YXJnZXQuZmlyc3RFbGVtZW50Q2hpbGQucmVtb3ZlKCk7XHJcblx0XHRcdFx0XHRyZXN1bHQuc3R5bGUudmlzaWJpbGl0eSA9ICdoaWRkZW4nO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fTtcclxuXHJcblx0XHRcdGVsZW0uYWRkRXZlbnRMaXN0ZW5lcignbW91c2VlbnRlcicsIGV2ZW50ID0+IGltYWdlWm9vbShldmVudCkpO1xyXG5cdFx0XHRlbGVtLnBhcmVudEVsZW1lbnQuYWRkRXZlbnRMaXN0ZW5lcignbW91c2VsZWF2ZScsIGV2ZW50ID0+IGltYWdlWm9vbUtpbGwoZXZlbnQpKTtcclxuXHRcdH1cclxuXHR9XHJcbn07XHJcbiIsIm1vZHVsZS5leHBvcnRzID0gY2xhc3MgQWRkTG9naWNDYXJvdXNlbCB7XHJcblx0Y29uc3RydWN0b3IoZW50ZXIsIGluaXQsIGdhcCwgZHJhZykge1xyXG5cdFx0dGhpcy5lbnRlciA9IGVudGVyO1xyXG5cdFx0dGhpcy5kcmFnID0gZHJhZztcclxuXHRcdHRoaXMuZ2FwID0gZ2FwO1xyXG5cdFx0dGhpcy5pbml0ID0ge1xyXG5cdFx0XHQwOiB7IGl0ZW1zOiBpbml0LnNtIH0sXHJcblx0XHRcdDc2ODogeyBpdGVtczogaW5pdC5tZCB9LFxyXG5cdFx0XHQ5OTI6IHsgaXRlbXM6IGluaXQubGcgfSxcclxuXHRcdFx0MTIwMDogeyBpdGVtczogaW5pdC54bCB9LFxyXG5cdFx0XHQxNjAwOiB7IGl0ZW1zOiBpbml0Lnh4bCB9XHJcblx0XHR9O1xyXG5cdH1cclxuXHJcblx0c3RhdGljIGluZm8oKSB7XHJcblx0XHRjb25zb2xlLmxvZygnTU9EVUxFOicsIHRoaXMubmFtZSwgdHJ1ZSk7XHJcblx0fVxyXG5cclxuXHRydW4oKSB7XHJcblx0XHRjb25zdCBlbGVtID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihgJHt0aGlzLmVudGVyfWApO1xyXG5cdFx0Ly8gY29uc3Qgb3V0cHV0ID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihgJHt0aGlzLmV4aXR9YCk7XHJcblx0XHRpZiAoZWxlbSkge1xyXG5cdFx0XHR0aGlzLmNvbnN0cnVjdG9yLmluZm8oKTtcclxuXHRcdFx0Y29uc3QgaW5pdCA9IHtcclxuXHRcdFx0XHRsb29wOiB0cnVlLFxyXG5cdFx0XHRcdG1hcmdpbjogdGhpcy5nYXAsXHJcblx0XHRcdFx0bmF2OiBmYWxzZSxcclxuXHRcdFx0XHRkb3RzOiBmYWxzZSxcclxuXHRcdFx0XHRyZXdpbmQ6IGZhbHNlLFxyXG5cdFx0XHRcdHRvdWNoRHJhZzogdGhpcy5kcmFnLFxyXG5cdFx0XHRcdG1vdXNlRHJhZzogdGhpcy5kcmFnLFxyXG5cdFx0XHRcdG1lcmdlOiBmYWxzZSxcclxuXHRcdFx0XHRyZXNwb25zaXZlOiB0aGlzLmluaXRcclxuXHRcdFx0fTtcclxuXHJcblx0XHRcdCQodGhpcy5lbnRlcikub3dsQ2Fyb3VzZWwoaW5pdCk7XHJcblx0XHRcdGVsZW0uc3R5bGUudmlzaWJpbGl0eSA9ICd2aXNpYmxlJztcclxuXHJcblx0XHRcdGNvbnN0IGNvbnRyb2wgPSAoZXZlbnQsIG5hbWUpID0+IHtcclxuXHRcdFx0XHRldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xyXG5cdFx0XHRcdCQodGhpcy5lbnRlcikudHJpZ2dlcihgJHtuYW1lfS5vd2wuY2Fyb3VzZWxgKTtcclxuXHRcdFx0fTtcclxuXHJcblx0XHRcdCQoYCR7dGhpcy5lbnRlcn1OYXZpZ2F0aW9uYCkuZXEoMSkub24oJ2NsaWNrJywgZXZlbnQgPT4gY29udHJvbChldmVudCwgJ25leHQnKSk7XHJcblx0XHRcdCQoYCR7dGhpcy5lbnRlcn1OYXZpZ2F0aW9uYCkuZXEoMCkub24oJ2NsaWNrJywgZXZlbnQgPT4gY29udHJvbChldmVudCwgJ3ByZXYnKSk7XHJcblx0XHR9XHJcblx0fVxyXG59O1xyXG4iLCJtb2R1bGUuZXhwb3J0cyA9IGNsYXNzIEFkZExvZ2ljRnVsbEltYWdlIHtcclxuXHRjb25zdHJ1Y3RvcihlbnRlciwgZXhpdCkge1xyXG5cdFx0dGhpcy5lbnRlciA9IGVudGVyO1xyXG5cdFx0dGhpcy5leGl0ID0gZXhpdDtcclxuXHR9XHJcblxyXG5cdHN0YXRpYyBpbmZvKCkge1xyXG5cdFx0Y29uc29sZS5sb2coJ01PRFVMRTonLCB0aGlzLm5hbWUsIHRydWUpO1xyXG5cdH1cclxuXHJcblx0cnVuKCkge1xyXG5cdFx0Y29uc3QgZWxlbSA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoYCR7dGhpcy5lbnRlcn1gKTtcclxuXHRcdGNvbnN0IG91dHB1dCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoYCR7dGhpcy5leGl0fWApO1xyXG5cdFx0aWYgKGVsZW0pIHtcclxuXHRcdFx0dGhpcy5jb25zdHJ1Y3Rvci5pbmZvKCk7XHJcblxyXG5cdFx0XHRjb25zdCBjbGlja2VkRnVuYyA9IChldmVudCkgPT4ge1xyXG5cdFx0XHRcdGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XHJcblx0XHRcdFx0Y29uc3QgY3VyRWxlbSA9IGV2ZW50LnRhcmdldDtcclxuXHRcdFx0XHRjb25zdCBnZXREYXRhID0gY3VyRWxlbS5zcmMgfHwgY3VyRWxlbS5maXJzdEVsZW1lbnRDaGlsZC5zcmM7XHJcblx0XHRcdFx0b3V0cHV0LnN0eWxlLmJhY2tncm91bmRJbWFnZSA9IGB1cmwoJHtnZXREYXRhfSlgO1xyXG5cdFx0XHR9O1xyXG5cclxuXHRcdFx0Y29uc3QgbWFpblVybCA9IFsuLi5lbGVtLnF1ZXJ5U2VsZWN0b3IoJy5vd2wtaXRlbScpLnBhcmVudEVsZW1lbnQuY2hpbGRyZW5dWzRdO1xyXG5cdFx0XHRjb25zdCB1cmwgPSBtYWluVXJsLnF1ZXJ5U2VsZWN0b3IoJ2ltZycpLnNyYztcclxuXHRcdFx0b3V0cHV0LnN0eWxlLmJhY2tncm91bmRJbWFnZSA9IGB1cmwoJHt1cmx9KWA7XHJcblx0XHRcdG91dHB1dC5zdHlsZS53aWR0aCA9IGAke291dHB1dC5wYXJlbnRFbGVtZW50Lm9mZnNldFdpZHRofXB4YDtcclxuXHJcblx0XHRcdCQoYCR7dGhpcy5lbnRlcn0gLmdhbGxlcnlfX2l0ZW1gKS5vbignY2xpY2snLCBldmVudCA9PiBjbGlja2VkRnVuYyhldmVudCkpO1xyXG5cdFx0fVxyXG5cdH1cclxufTtcclxuIiwibW9kdWxlLmV4cG9ydHMgPSBjbGFzcyBBZGRNb3ZlU2Nyb2xsVVAge1xyXG5cdGNvbnN0cnVjdG9yKGl0ZW0sIHNwZWVkKSB7XHJcblx0XHR0aGlzLml0ZW0gPSBpdGVtO1xyXG5cdFx0dGhpcy5zcGVlZCA9IHNwZWVkO1xyXG5cdH1cclxuXHJcblx0c3RhdGljIGluZm8oKSB7XHJcblx0XHRjb25zb2xlLmxvZygnTU9EVUxFOicsIHRoaXMubmFtZSwgdHJ1ZSk7XHJcblx0fVxyXG5cclxuXHRydW4oKSB7XHJcblx0XHRjb25zdCBlbGVtID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihgJHt0aGlzLml0ZW19YCk7XHJcblx0XHRpZiAoZWxlbSkge1xyXG5cdFx0XHR0aGlzLmNvbnN0cnVjdG9yLmluZm8oKTtcclxuXHRcdFx0dGhpcy5zcGVlZCA9IHRoaXMuc3BlZWQgPT09ICdmYXN0JyA/IDggLyAyIDogODtcclxuXHRcdFx0dGhpcy5zcGVlZCA9IHRoaXMuc3BlZWQgPT09ICdzbG93JyA/IDggKiAyIDogODtcclxuXHJcblx0XHRcdGNvbnN0IHNjcm9sbE1lID0gKCkgPT4ge1xyXG5cdFx0XHRcdGNvbnN0IGdldFNjcm9sbCA9IGRvY3VtZW50LmRvY3VtZW50RWxlbWVudC5zY3JvbGxUb3AgfHwgZG9jdW1lbnQuYm9keS5zY3JvbGxUb3A7XHJcblx0XHRcdFx0aWYgKGdldFNjcm9sbCA+IDApIHtcclxuXHRcdFx0XHRcdHdpbmRvdy5yZXF1ZXN0QW5pbWF0aW9uRnJhbWUoc2Nyb2xsTWUpO1xyXG5cdFx0XHRcdFx0d2luZG93LnNjcm9sbFRvKDAsIGdldFNjcm9sbCAtIGdldFNjcm9sbCAvIHRoaXMuc3BlZWQpO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fTtcclxuXHRcdFx0ZWxlbS5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsICgpID0+IHNjcm9sbE1lKCkpO1xyXG5cdFx0fVxyXG5cdH1cclxufTtcclxuIiwibW9kdWxlLmV4cG9ydHMgPSBjbGFzcyBBZGRQbGF5WW91dHViZSB7XHJcblx0Y29uc3RydWN0b3IoaXRlbSwgbnVtYmVyLCBkcmFnKSB7XHJcblx0XHR0aGlzLml0ZW0gPSBpdGVtO1xyXG5cdFx0dGhpcy5udW1iZXIgPSBudW1iZXI7XHJcblx0XHR0aGlzLmRyYWcgPSBkcmFnO1xyXG5cdH1cclxuXHJcblx0c3RhdGljIGluZm8oKSB7XHJcblx0XHRjb25zb2xlLmxvZygnTU9EVUxFOicsIHRoaXMubmFtZSwgdHJ1ZSk7XHJcblx0fVxyXG5cclxuXHRydW4oKSB7XHJcblx0XHRjb25zdCBlbGVtID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihgJHt0aGlzLml0ZW19YCk7XHJcblx0XHRpZiAoZWxlbSkge1xyXG5cdFx0XHR0aGlzLmNvbnN0cnVjdG9yLmluZm8oKTtcclxuXHRcdFx0XHJcblx0XHRcdGNvbnN0IGRvaXQgPSAoZXZlbnQpID0+IHtcclxuXHRcdFx0XHRjb25zdCBxd2UgPSAkKCcjeW91dHViZS1zdGFydC11cmwnKS5hdHRyKCdkYXRhLWxhenktc3JjJyk7XHJcblx0XHRcdFx0JCgnI3lvdXR1YmUtc3RhcnQtdXJsJykuYXR0cignc3JjJywgcXdlKTtcclxuXHRcdFx0XHQkKGAke3RoaXMuaXRlbX1gKS5mYWRlT3V0KCk7XHJcblx0XHRcdH07XHJcblx0XHRcdFxyXG5cdFx0XHRlbGVtLmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgZXZlbnQgPT4gZG9pdChldmVudCkpO1xyXG5cdFx0fVxyXG5cdH1cclxufTtcclxuIiwibW9kdWxlLmV4cG9ydHMgPSBjbGFzcyBBZGR0YWJTd2l0Y2hlciB7XHJcblx0Y29uc3RydWN0b3IoZW50ZXIsIGV4aXQpIHtcclxuXHRcdHRoaXMuZW50ZXIgPSBlbnRlcjtcclxuXHRcdHRoaXMuZXhpdCA9IGV4aXQ7XHJcblx0fVxyXG5cclxuXHRzdGF0aWMgaW5mbygpIHtcclxuXHRcdGNvbnNvbGUubG9nKCdNT0RVTEU6JywgdGhpcy5uYW1lLCB0cnVlKTtcclxuXHR9XHJcblxyXG5cdHJ1bigpIHtcclxuXHRcdGNvbnN0IGVsZW0gPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKGAke3RoaXMuZW50ZXJ9YCk7XHJcblx0XHRjb25zdCBvdXRwdXQgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKGAke3RoaXMuZXhpdH1gKTtcclxuXHRcdGlmIChlbGVtKSB7XHJcblx0XHRcdHRoaXMuY29uc3RydWN0b3IuaW5mbygpO1xyXG5cclxuXHRcdFx0Y29uc3QgbG9naWMgPSAoZXZlbnQpID0+IHtcclxuXHRcdFx0XHRldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xyXG5cdFx0XHRcdGNvbnN0IGN1ckVsZW0gPSBldmVudC50YXJnZXQ7XHJcblx0XHRcdFx0Y29uc3QgaW5kZXggPSBbLi4uY3VyRWxlbS5wYXJlbnRFbGVtZW50LmNoaWxkcmVuXS5pbmRleE9mKGN1ckVsZW0pO1xyXG5cclxuXHRcdFx0XHRpZiAob3V0cHV0LnF1ZXJ5U2VsZWN0b3IoJy5hY3RpdmUnKSkge1xyXG5cdFx0XHRcdFx0b3V0cHV0LnF1ZXJ5U2VsZWN0b3IoJy5hY3RpdmUnKS5jbGFzc0xpc3QucmVtb3ZlKCdhY3RpdmUnKTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdFx0b3V0cHV0LmNoaWxkcmVuW2luZGV4XS5jbGFzc0xpc3QuYWRkKCdhY3RpdmUnKTtcclxuXHRcdFx0fTtcclxuXHJcblx0XHRcdG91dHB1dC5maXJzdEVsZW1lbnRDaGlsZC5jbGFzc0xpc3QuYWRkKCdhY3RpdmUnKTtcclxuXHRcdFx0ZWxlbS5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIGV2ZW50ID0+IGxvZ2ljKGV2ZW50KSk7XHJcblx0XHR9XHJcblx0fVxyXG59O1xyXG4iXSwibWFwcGluZ3MiOiI7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7OztBQ3RKQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUVBOzs7Ozs7Ozs7Ozs7O0FDSkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUdBO0FBQ0E7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBRkE7QUFNQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUN6Q0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBUEE7QUFBQTtBQUFBO0FBWUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFKQTtBQU9BO0FBRUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBRUE7QUFFQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUE1Q0E7QUFBQTtBQUFBO0FBU0E7QUFDQTtBQVZBO0FBQ0E7QUFEQTtBQUFBOzs7Ozs7Ozs7Ozs7Ozs7OztBQ0FBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFBQTtBQUFBO0FBV0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFFQTtBQUNBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFqRkE7QUFBQTtBQUFBO0FBT0E7QUFDQTtBQVJBO0FBQ0E7QUFEQTtBQUFBOzs7Ozs7Ozs7Ozs7Ozs7OztBQ0FBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBTEE7QUFPQTtBQUNBO0FBYkE7QUFBQTtBQUFBO0FBa0JBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBVEE7QUFZQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQTlDQTtBQUFBO0FBQUE7QUFlQTtBQUNBO0FBaEJBO0FBQ0E7QUFEQTtBQUFBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ0FBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFBQTtBQUFBO0FBV0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBRUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQTlCQTtBQUFBO0FBQUE7QUFPQTtBQUNBO0FBUkE7QUFDQTtBQURBO0FBQUE7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ0FBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFBQTtBQUFBO0FBVUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQTFCQTtBQUFBO0FBQUE7QUFPQTtBQUNBO0FBUkE7QUFDQTtBQURBO0FBQUE7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQU5BO0FBQUE7QUFBQTtBQVdBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQXhCQTtBQUFBO0FBQUE7QUFRQTtBQUNBO0FBVEE7QUFDQTtBQURBO0FBQUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQUFBO0FBQUE7QUFXQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQTlCQTtBQUFBO0FBQUE7QUFPQTtBQUNBO0FBUkE7QUFDQTtBQURBO0FBQUE7Ozs7QSIsInNvdXJjZVJvb3QiOiIifQ==